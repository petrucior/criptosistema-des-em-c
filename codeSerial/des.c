/**
 * \file des.cpp
 *
 * \brief Este arquivo contém a implementação dos métodos do criptosistema Data Encryption Standard (DES).
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.2
 * \date Junho 2014
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "des.h"
#include "msg.h"
#include "convertMsg.h"
#include "blocos.h"
#include "permutacao.h"
#include "keys.h"
#include "funcao.h"
#ifdef _OPENMP
	#include <omp.h>
#endif

#define TAMANHO 259200 /* Tamanho do texto após o preenchimento com zeros */

int main(int argc, char* argv[]){
	int encriptado[TAMANHO];
	const double tempoInicio = omp_get_wtime();
	des_encrypt(encriptado);
	const double tempoFinal = omp_get_wtime();
	char hexadecimal[TAMANHO/4];
	convertBinToHex(hexadecimal, encriptado, TAMANHO);
	
	/*printf("Encriptado\n");
	int v;
	for (v = 0; v < TAMANHO/4; v++)
		printf("%c", hexadecimal[v]);
	printf("\n");*/

	int decriptado[TAMANHO];
	des_decrypt(decriptado, encriptado, TAMANHO);
	convertBinToHex(hexadecimal, decriptado, TAMANHO);

	/*printf("Decriptado\n");
	for (v = 0; v < TAMANHO/4; v++)
		printf("%c", hexadecimal[v]);
	printf("\n");*/

	printf("tempo percorrido: %f\n", tempoFinal - tempoInicio);

	return 0;
}

/**
 * \fn void des_encrypt(int* encriptado);
 *
 * \brief Método de encriptação.
 *
 * \param encriptado - ponteiro com informação criptografada.
 */
 void 
 des_encrypt(int* encriptado){
	int i, j;
	
	/* Convertendo o texto para binário */
	int tam = (int) strlen(mensagem)*8;
	int binario[tam];
	convertTextToBin(binario, mensagem, tam/8);

	/*for (i = 0; i < tam; i++){
		printf("%c", mensagem[i]);
	}
	printf("\n");*/
	
	/* Quebrando a mensagem binária em blocos */
	int tamanho = 0;
	if (tam % 64 != 0) tamanho = (int) tam + (64 - (tam % 64));
	else tamanho = tam;
	int bls[tamanho];
	quebraBin2Bls(bls, binario, tam);

	/* Construção de subchaves */
	setSubKeys();
	
	int bloco[64];
	int contador = 0;
	/* Para cada bloco é realizada a encriptação */
	for (i = 0; i < tamanho/64; i++){
		/* Preenchimento de cada bloco */
		extrairBls(bloco, bls, i);

		/*for (i = 0; i < tamanho; i++)
			printf("%d", bloco[i]);
		printf("\n");*/
		
		/* Permutação inicial */
		int perm[64];
		permutacaoInicial(perm, bloco);

		/* Quebra o resultado permutado em duas partes: left e right */
		int left[32], right[32];
		quebra32bits(left, right, perm, 0);

		for (j = 0; j < 16; j++){
			/* Pegando a subChave */
			int subChave[48];
			getSubKey(subChave, j);

			/* Função de feistel */
			int saidaFeistel[32];
			f(saidaFeistel, right, subChave);

			/* Função xor */
			int aux[32];
			copiar(aux, right, 32);
			operacaoXor(right, left, saidaFeistel, 32);
			copiar(left, aux, 32);
		}
		
		/* Concatenacao com swap de right com left (preoutput) */
		int concatenado[64];
		concatenacao(concatenado, right, left, 32, 32);

		/* Permutação final */
		int permutF[64];
		permutacaoFinal(permutF, concatenado);

		/* Concatenando com o ponteiro de saída encriptada */
		concatenacao(encriptado, encriptado, permutF, 64*contador, 64);
		
		contador++;
	} /* Termino do laço for */
 }

/**
 * \fn void des_decrypt(int* decriptado, int* encriptado, int tamanho);
 *
 * \brief Método de encriptação.
 *
 * \param decriptado - ponteiro com a mensagem decriptada.
 *         encriptado - ponteiro com informação criptografada.
 *         tamanho - Tamanho da mensagem encriptada.
 */
 void 
 des_decrypt(int* decriptado, int* encriptado, int tamanho){
	/* Quebrando a mensagem binária em blocos */
	int bls[tamanho];
	quebraBin2Bls(bls, encriptado, tamanho);

	/* Construção de subchaves */
	setSubKeys();

	int bloco[64];
	int contador = 0;
	int i, j;
	/* Para cada bloco é realizada a decriptação */
	for (i = 0; i < tamanho/64; i++){
		/* Preenchimento de cada bloco */
		extrairBls(bloco, bls, i);

		/* Permutação inicial */
		int perm[64];
		permutacaoInicial(perm, bloco);

		/* Quebra o resultado permutado em duas partes: right e left */
		int left[32], right[32];
		quebra32bits(right, left, perm, 0);

		for (j = 0; j < 16; j++){
			/* Pegando a subChave */
			int subChave[48];
			getSubKey(subChave, 16 - j - 1);

			/* Função de feistel */
			int saidaFeistel[32];
			f(saidaFeistel, left, subChave);

			/* Função xor */
			int aux[32];
			copiar(aux, left, 32);
			operacaoXor(left, right, saidaFeistel, 32);
			copiar(right, aux, 32);
		}
		
		/* Concatenacao com swap de left com right (preoutput) */
		int concatenado[64];
		concatenacao(concatenado, left, right, 32, 32);

		/* Permutação final */
		int permutF[64];
		permutacaoFinal(permutF, concatenado);

		/* Concatenando com o ponteiro de saída encriptada */
		concatenacao(decriptado, decriptado, permutF, 64*contador, 64);
		
		contador++;
	} /* Termino do laço for */
 }
