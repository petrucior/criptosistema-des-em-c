/**
 * \file keys.h
 *
 * \brief Este arquivo contem as chaves do algoritmo DES.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Junho 2014
 */

#ifndef _KEYS_H_
#define _KEYS_H_

#include <stdlib.h>
#include <stdio.h>


 /**
  * \fn void gerarKeyPrivada();
  *
  * \brief Gera uma chave privada de 64 bits.
  */
  void gerarKeyPrivada();

 /**
  * \fn void getKeyPrivada();
  *
  * \brief Pega a chave privada guardada em um arquivo.
  *
  * \param chave - ponteiro do vetor que contem a chave.
  */
  void getKeyPrivada();

 /**
  * \fn void deslocamento(int* deslocado, int* conteudo, int tamanho);
  *
  * \brief Faz um shift de tamanho tam a esquerda do dado.
  *
  * \param deslocado - ponteiro para a informação deslocada.
  *        conteudo - ponteiro para o conteudo que será deslocado.
  *        tamanho - tamanho do deslocamento que a informação será submetida.
  */
  void deslocamento(int* deslocado, int* conteudo, int tamanho);

 /**
  * \fn void setSubKeys();
  *
  * \brief Gera as sub-chaves a partir da chave privada.
  */
  void setSubKeys();

 /**
  * \fn void getSubKey(int* subChave, int posicao);
  *
  * \brief Pega a sub-chave correspondente a posição dela.
  *
  * \param subChave - ponteiro para armazenar a subchave.
  *        posicao - indicador de qual subchave será pego.
  */
  void getSubKey(int* subChave, int posicao);	 

  /* Atributos privados */
  int chave[64];
  int subChaves[16*48];

#endif
