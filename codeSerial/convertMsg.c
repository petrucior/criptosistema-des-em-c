/**
 * \file convertMsg.c
 *
 * \brief Este arquivo contém as implementações das funções de conversões das mensagens.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.2
 * \date Maio 2014
 */

#include "convertMsg.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

 /**
  * \fn void convertMsgToBin(int* binario, int valor, int tamanho);
  *
  * \brief Função que converte um inteiro em binário.
  *
  * \param binario - ponteiro de um vetor de inteiros onde armazenará o valor binário.
  *        valor - contém o valor que desejamos converter para binário.
  *        tamanho - quantidade de bits a ser colocado no vetor binário.
  *
  * \return O endereço de vetor correspondente a representação em binário do valor inteiro.
  */
 void
 convertMsgToBin(int* binario, int valor, int tamanho){
	int i;
	for (i = 0; i < tamanho; i++)
		binario[i] = 0;
	i = 0;
	while(valor > 0){
		binario[(tamanho - 1) - i] = valor % 2;
    		valor = (int) valor / 2;
    		i++;
	}
	return;
 }

 /**
  * \fn void convertTextToBin(int* textoBinario, char* texto, int tamanhoTexto);
  *
  * \brief Função que converte um texto em binário.
  *
  * \param textoBinario - ponteiro com saída do texto binário.
  *        texto - contém a informação que desejamos converter para binário.
  *        tamanho - a quantidade de caracteres do texto.
  * \return O endereço de vetor correspondente a representação em binário do texto.
  */
 void
 convertTextToBin(int* textoBinario, char* texto, int tamanhoTexto){
	int i = 0;
	int contador = 0;
	while (contador < tamanhoTexto){
		int auxiliar[8];
		int j;
		convertMsgToBin(auxiliar, (int)texto[contador], 8);
		for (j = 0; j < 8; j++){
			int posicao = contador * 8 + j;
			textoBinario[posicao] = auxiliar[j];
		}

		i++;
		contador++;
	}
	return;
 }


/**
 * \fn int convertBinToNum(char* binario, int tamanho);
 *
 * \brief Função que converte de binário para inteiro.
 *
 * \param binario - contém a informação que desejamos converter para binário.
 *        tamanho - a quantidade de bits contida no binário.
 *
 * \return O valor convertido em inteiro.
 */
 int 
 convertBinToNum(int* binario, int tamanho){
	int decimal = 0;
	int i;
	for (i = 0; i < tamanho; i++)
		decimal += binario[i] * pow(2, (tamanho - 1) - i); 
	return decimal;
 }

/**
 * \fn void extraiInfo(int* vetor, int* binario, int posicao);
 *
 * \brief Pegar informação de um vetor binário.
 *
 * \param vetor - ponteiro onde colocaremos a informação extraida.
 *        binario - contém a informação que desejamos pegar.
 *        posicao - lugar de onde vamos começar a pegar a informação.
 */
 void 
 extraiInfo(int* vetor, int* binario, int posicao){
	int i;
	for (i = 0; i < 4; i++)
		vetor[i] = binario[i + 4*posicao];
 }

/**
 * \fn void convertBinToHex(char* hexadecimal, char* binario, int tamanho);
 *
 * \brief Função que converte de binário para hexadecimal.
 *
 * \param hexadecimal - ponteiro onde colocaremos a informação convertida.
 *        binario - contém a informação que desejamos converter para binário.
 *        tamanho - a quantidade de bits contida no binário.
 */
 void
 convertBinToHex(char* hexadecimal, int* binario, int tamanho){
	int aux[4];
	int contador = 0;
	int i;
	for (i = 0; i < 4; i++) aux[i] = 0;
	for (i = 0; i < tamanho/4; i++){
		extraiInfo(aux, binario, i);
		if ((aux[0] == 0) && (aux[1] == 0) && (aux[2] == 0) && (aux[3] == 0))
			hexadecimal[contador] = '0';
		if ((aux[0] == 0) && (aux[1] == 0) && (aux[2] == 0) && (aux[3] == 1))
			hexadecimal[contador] = '1';
		if ((aux[0] == 0) && (aux[1] == 0) && (aux[2] == 1) && (aux[3] == 0))
			hexadecimal[contador] = '2';
		if ((aux[0] == 0) && (aux[1] == 0) && (aux[2] == 1) && (aux[3] == 1))
			hexadecimal[contador] = '3';
		if ((aux[0] == 0) && (aux[1] == 1) && (aux[2] == 0) && (aux[3] == 0))
			hexadecimal[contador] = '4';
		if ((aux[0] == 0) && (aux[1] == 1) && (aux[2] == 0) && (aux[3] == 1))
			hexadecimal[contador] = '5';
		if ((aux[0] == 0) && (aux[1] == 1) && (aux[2] == 1) && (aux[3] == 0))
			hexadecimal[contador] = '6';
		if ((aux[0] == 0) && (aux[1] == 1) && (aux[2] == 1) && (aux[3] == 1))
			hexadecimal[contador] = '7';
		if ((aux[0] == 1) && (aux[1] == 0) && (aux[2] == 0) && (aux[3] == 0))
			hexadecimal[contador] = '8';
		if ((aux[0] == 1) && (aux[1] == 0) && (aux[2] == 0) && (aux[3] == 1))
			hexadecimal[contador] = '9';
		if ((aux[0] == 1) && (aux[1] == 0) && (aux[2] == 1) && (aux[3] == 0))
			hexadecimal[contador] = 'a';
		if ((aux[0] == 1) && (aux[1] == 0) && (aux[2] == 1) && (aux[3] == 1))
			hexadecimal[contador] = 'b';
		if ((aux[0] == 1) && (aux[1] == 1) && (aux[2] == 0) && (aux[3] == 0))
			hexadecimal[contador] = 'c';
		if ((aux[0] == 1) && (aux[1] == 1) && (aux[2] == 0) && (aux[3] == 1))
			hexadecimal[contador] = 'd';
		if ((aux[0] == 1) && (aux[1] == 1) && (aux[2] == 1) && (aux[3] == 0))
			hexadecimal[contador] = 'e';
		if ((aux[0] == 1) && (aux[1] == 1) && (aux[2] == 1) && (aux[3] == 1))
			hexadecimal[contador] = 'f';
		contador++;
	}
 }


