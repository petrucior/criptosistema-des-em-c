/**
 * \file keys.cpp
 *
 * \brief Este arquivo contem as Implementação das chaves do algoritmo DES.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.2
 * \date Junho 2014
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "keys.h"
#include "permutacao.h"
#include "funcao.h"

/**
 * \fn void gerarKeyPrivada();
 * 
 * \brief Gera uma chave privada de 64 bits.
 */
 void 
 gerarKeyPrivada(){
	int valores[64];	
	int i;
	FILE* arquivo;
	char* nome = "keys.txt";
	if ((arquivo = fopen(nome, "r+")) == NULL){
		printf("erro ao abrir o arquivo que contém a chave privada\n");
		return; 
	}
	for (i = 0; i < 64; i++){
		valores[i] = floor(rand() % 2); /* Gerando um valor inteiro entre 0 e 1 randomicamente */
	}
	for (i = 0; i < 64; i++){
		fputc(valores[i], arquivo);
	}
	fclose(arquivo); /* Fechando arquivo */
	return;
 }

/**
 * \fn void getKeyPrivada();
 *
 * \brief Pega a chave privada guardada em um arquivo.
 *
 * \param chave - ponteiro do vetor que contem a chave.
 */
 void 
 getKeyPrivada(){
	int i;
	int valor;
	FILE* arquivo;
	char* nome = "keys.txt";
	if ((arquivo = fopen(nome, "r+")) == NULL){
		printf("erro ao abrir o arquivo que contém a chave privada\n");
		return;
	}
	
	for (i = 0; i < 64; i++){
		valor = fgetc(arquivo);
		/* Verificando de acordo com a tabela ascii - 0 = 48 e 1 = 49 */
		if (valor == 48)
			chave[i] = 0;
		else
			chave[i] = 1;
	}

	fclose(arquivo);
	return;
 }

/**
 * \fn void deslocamento(int* deslocado, int tamanho);
 *
 * \brief Faz um shift de tamanho tam a esquerda do dado.
 *
 * \param deslocado - ponteiro para a informação deslocada.
 *        conteudo - ponteiro para o conteudo que será deslocado.
 *        tamanho - tamanho do deslocamento que a informação será submetida.
 */
 void 
 deslocamento(int* deslocado, int* conteudo, int tamanho){
	int aux[4];
	int i;
	for (i = 0; i < tamanho; i++)
		aux[i] = conteudo[i];
	for (i = 0; i < 28 - tamanho; i++)
		deslocado[i] = conteudo[i + tamanho];
	for (i = 0; i < tamanho; i++)
		deslocado[28 - tamanho + i] = aux[i];

 }

/**
 * \fn void setSubKeys();
 *
 * \brief Gera as sub-chaves a partir da chave privada.
 */
 void 
 setSubKeys(){
	/* Método que pega a chave privada */
 	getKeyPrivada();
	/* Permutações e separação em c e d */
	int c[28], d[28];
	permutacaoPC1_C(c, chave);
	permutacaoPC1_D(d, chave);
	int i;
	for (i = 0; i < 16; i++){
		/* Shift à esquerda em c e d */
		deslocamento(c, c, getDesloc(i));
		deslocamento(d, d, getDesloc(i));
		
		/* Concatenação */
		int c_dDeslocados[56];
		concatenacao(c_dDeslocados, c, d, 28, 28);

		int subChave[48];
		permutacaoPC2(subChave, c_dDeslocados);
		int j;
		for (j = 0; j < 48; j++)
			subChaves[j + i*48] = subChave[j];
	}
 }

/**
 * \fn void getSubKey(int* subChave, int posicao);
 *
 * \brief Pega a sub-chave correspondente a posição dela.
 *
 * \param subChave - ponteiro para armazenar a subchave.
 *        posicao - indicador de qual subchave será pego.
 */
 void 
 getSubKey(int* subChave, int posicao){
	int i;
	for (i = 0; i < 48; i++)
		subChave[i] = subChaves[i + 48*posicao];
 }




