/**
 * \file blocos.h
 *
 * \brief Este arquivo contém os blocos da mensagem.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Maio 2014
 */

#ifndef _BLOCOS_H_
#define _BLOCOS_H_

#include <stdlib.h>
#include <stdio.h>

 /**
  * \fn void quebraBin2Bls(int* blocos, int* binario, int tamanho);
  *
  * \brief Função que quebra a informação binária em blocos.
  *
  * \param blocos - ponteiro onde vamos adicionar os vetores binários.
  *        binario - ponteiro com informação binária.
  *
  * \return Quebra a informação binária em blocos de 64 bits para ser utilizado no DES.
  */
  void quebraBin2Bls(int* blocos, int* binario, int tamanho);

/**
 * \fn void quebra32bits(int* left, int* right, int* blocos, interacao);
 *
 * \brief Função que quebra o bloco em dois subblocos de 32 bits.
 *
 * \param left - ponteiro para armazenar os 32 bits mais a esquerda.
 *        right - ponteiro para armazenar os 32 bits mais a direita.
 *        blocos - ponteiro onde vamos adicionar os vetores binários.
 *        interacao - identificador de qual bloco será quebrado (1 - quantidade de blocos).
 *
 * \return Quebra a informação binária em blocos de 64 bits para ser utilizado no DES.
 */
 void quebra32bits(int* left, int* right, int* blocos, int interacao);

/**
 * \fn void quebraBin2Bls(int* bloco, int* blocos, int posicao);
 *
 * \brief Extrai o bloco especificado por posicao.
 *
 * \param bloco - ponteiro onde vamos adicionar os vetores binários.
 *        blocos - ponteiro com informação binária.
 *        posicao - lugar de onde vamos começar a pegar a informação.
 */
 void extrairBls(int* bloco, int* blocos, int posicao);

#endif
