/**
 * \file funcao.h
 *
 * \brief Contém a assinatura da função de feistel.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Maio 2014
 */

#ifndef _FUNCAO_H_
#define _FUNCAO_H_

#include <stdlib.h>
#include <stdio.h>

 /**
  * \fn void operacaoXor(int* saida, int* x, int* y, int tamanho);
  *
  * \brief Realiza a operacao xor
  *
  * \param saida - ponteiro de saida da xor.
  *        x, y - ponteiros com informaçoes que deverão passar pela xor.
  *        tamanho - a quantidade dos ponteiros.
  */
  void operacaoXor(int* saida, int* x, int* y, int tamanho);

 /**
  * \fn void concatenacao(int* saida, int* x, int* y, int tamanhox, int tamanhoy);
  *
  * \brief Realiza a operacao de concatenacao.
  *
  * \param saida - ponteiro de saida da concatenacao.
  *        x, y - ponteiros com informaçoes que deverão ser concatenada.
  *        tamanho - a quantidade dos ponteiros.
  */
  void concatenacao(int* saida, int* x, int* y, int tamanhox, int tamanhoy);

 /**
  * \fn void copiar(int* saida, int* x, int tamanho);
  *
  * \brief Realiza uma copia do vetor x.
  *
  * \param saida - ponteiro de saida da concatenacao.
  *        x - ponteiro com informaçoes que deverão ser copiada.
  *        tamanho - a quantidade dos ponteiros.
  */
  void copiar(int* saida, int* x, int tamanho);

 /**
  * \fn void extrairVetor(int* vetor, int* x, int posicao);
  *
  * \brief Realiza uma extração de 6 bits do vetor x.
  *
  * \param vetor - ponteiro de saida da extração.
  *        x - ponteiro com informaçoes que deverão ser extraida.
  *        posicao - posicao de inicio da extração.
  */
  void extrairVetor(int* vetor, int* x, int posicao);

 /**
  * \fn void f(int* saida, int* right, int* k);
  *
  * \brief Função de feistel.
  *
  * \param saida - ponteiro atualizado da saida de feistel.
  *        right - ponteiro com os 32 bits da parte direita.
  *        k - ponteiro com 64 bits da chave.
  *
  * \return A saída da função de feistel.
  */
  void f(int* saida, int* right, int* k);

#endif
