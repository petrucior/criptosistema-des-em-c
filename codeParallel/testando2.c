#include <stdlib.h>
#include <stdio.h>
#include "des.h"
#include "convertMsg.h"
#include <omp.h>

int main(){
	
	int encriptado[1600];
	des_encrypt(encriptado);
	char hexadecimal[1600/4];
	convertBinToHex(hexadecimal, encriptado, 1600);
	
	printf("Encriptado\n");
	int v;
	for (v = 0; v < 1600/4; v++)
		printf("%c", hexadecimal[v]);
	printf("\n");

	int decriptado[1600];
	des_decrypt(decriptado, encriptado, 1600);
	convertBinToHex(hexadecimal, decriptado, 1600);

	printf("Decriptado\n");
	for (v = 0; v < 1600/4; v++)
		printf("%c", hexadecimal[v]);
	printf("\n");

	return 0;
}
