/**
 * \file funcao.cpp
 *
 * \brief Implementação das função de feistel.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.2
 * \date Junho 2014
 */

#include <stdlib.h>
#include <stdio.h>
#include "funcao.h"
#include "convertMsg.h"
#include "permutacao.h"

/**
 * \fn void operacaoXor(int* saida, int* x, int* y, int tamanho);
 *
 * \brief Realiza a operacao xor
 *
 * \param saida - ponteiro de saida da xor.
 *        x, y - ponteiros com informaçoes que deverão passar pela xor.
 */
 void 
 operacaoXor(int* saida, int* x, int* y, int tamanho){
	int i;
	for (i = 0; i < tamanho; i++){
		if ((x[i] ^ y[i]) == 0){ saida[i] = 0; }
		else{ saida[i] = 1; }
	}
 }

/**
 * \fn void concatenacao(int* saida, int* x, int* y, int tamanhox, int tamanhoy);
 *
 * \brief Realiza a operacao de concatenacao.
 *
 * \param saida - ponteiro de saida da concatenacao.
 *        x, y - ponteiros com informaçoes que deverão ser concatenada.
 *        tamanho - a quantidade dos ponteiros x e y.
 */
 void 
 concatenacao(int* saida, int* x, int* y, int tamanhox, int tamanhoy){
	int i;
	for (i = 0; i < (tamanhox + tamanhoy); i++){
		if (i < tamanhox) saida[i] = x[i];
		else saida[i] = y[i - tamanhox];
	}
 }

/**
 * \fn void copiar(int* saida, int* x, int tamanho);
 *
 * \brief Realiza uma copia do vetor x.
 *
 * \param saida - ponteiro de saida da concatenacao.
 *        x - ponteiro com informaçoes que deverão ser copiada.
 *        tamanho - a quantidade dos ponteiros.
 */
 void 
 copiar(int* saida, int* x, int tamanho){
	int i;
	for (i = 0; i < tamanho; i++)
		saida[i] = x[i];	
 }

/**
 * \fn void extrairVetor(int* vetor, int* x, int posicao);
 *
 * \brief Realiza uma extração de 6 bits do vetor x.
 *
 * \param vetor - ponteiro de saida da extração.
 *        x - ponteiro com informaçoes que deverão ser extraida.
 *        posicao - posicao de inicio da extração.
 */
 void 
 extrairVetor(int* vetor, int* x, int posicao){
	int i;
	for (i = 0; i < 6; i++)
		vetor[i] = x[i + 6*posicao];
 }

/**
 * \fn void f(int* saida, int* right, int* k);
 *
 * \brief Função de feistel.
 *
 * \param saida - ponteiro atualizado da saida de feistel.
 *        right - ponteiro com os 32 bits da parte direita.
 *        k - ponteiro com 64 bits da chave.
 *
 * \return A saída da função de feistel.
 */
 void 
 f(int* saida, int* right, int* k){
	int expansao[48];
	expande48bits(expansao, right);
	int saidaXor[48];
	operacaoXor(saidaXor, expansao, k, 48);
	int b[6];
	int contador = 1;
	int bin[4];
	int saidaSBOX[32];
	
	int aux2bits[4];
	int aux4bits[4];
	int i;
	for (i = 0; i < 8; i++){
		extrairVetor(b, saidaXor, i);
		aux2bits[0] = 0;
		aux2bits[1] = 0;
		aux2bits[2] = b[0];
		aux2bits[3] = b[5];

		aux4bits[0] = b[1];
		aux4bits[1] = b[2];
		aux4bits[2] = b[3];
		aux4bits[3] = b[4];
	
		int linha = convertBinToNum(aux2bits, 4);
		int coluna = convertBinToNum(aux4bits, 4);
		switch (contador){
			int v;
			case 1:
				convertMsgToBin(bin, getValueS(coluna, linha, 1), 4);
				for (v = 0; v < 4; v++)
					saidaSBOX[v] = bin[v];
				break;
			case 2:
				convertMsgToBin(bin, getValueS(coluna, linha, 2), 4);
				for (v = 4; v < 8; v++)
					saidaSBOX[v] = bin[v - 4];
				break;
			case 3:
				convertMsgToBin(bin, getValueS(coluna, linha, 3), 4);
				for (v = 8; v < 12; v++)
					saidaSBOX[v] = bin[v - 8];
				break;
			case 4:
				convertMsgToBin(bin, getValueS(coluna, linha, 4), 4);
				for (v = 12; v < 16; v++)
					saidaSBOX[v] = bin[v - 12];
				break;
			case 5:
				convertMsgToBin(bin, getValueS(coluna, linha, 5), 4);
				for (v = 16; v < 20; v++)
					saidaSBOX[v] = bin[v - 16];
				break;
			case 6:
				convertMsgToBin(bin, getValueS(coluna, linha, 6), 4);
				for (v = 20; v < 24; v++)
					saidaSBOX[v] = bin[v - 20];
				break;
			case 7:
				convertMsgToBin(bin, getValueS(coluna, linha, 7), 4);
				for (v = 24; v < 28; v++)
					saidaSBOX[v] = bin[v - 24];
				break;
			case 8:
				convertMsgToBin(bin, getValueS(coluna, linha, 8), 4);
				for (v = 28; v < 32; v++)
					saidaSBOX[v] = bin[v - 28];
				break;
		}
		contador++;
	}
	permutacaoP(saida, saidaSBOX);
 }

