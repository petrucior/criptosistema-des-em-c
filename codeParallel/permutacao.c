/**
 * \file permutacao.c
 *
 * \brief Implementação das funções de permutação do DES.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Junho 2014
 */

#include <stdlib.h>
#include <stdio.h>
#include "tabelas.h"
#include "permutacao.h"

/**
 * \fn void permutacaoInicial(int* permutado, int* bloco);
 *
 * \brief Realiza a permutacao inicial
 *
 * \param permutado - ponteiro com informação permutada.
 *        bloco - ponteiro do bloco.
 */
 void 
 permutacaoInicial(int* permutado, int* bloco){
	int i;
	int tamanho = (sizeof(ip)/sizeof(ip[0]));
	for (i = 0; i < tamanho; i++)
		permutado[i] = bloco[ip[i] - 1];
 }

/**
 * \fn void permutacaoFinal(int* permutado, int* bloco);
 *
 * \brief Realiza a permutacao final
 *
 * \param permutado - ponteiro com informação permutada.
 *        bloco - ponteiro do bloco.
 */
 void 
 permutacaoFinal(int* permutado, int* bloco){
	int i;
	int tamanho = (sizeof(ip_1)/sizeof(ip_1[0]));
	for (i = 0; i < tamanho; i++)
		permutado[i] = bloco[ip_1[i] - 1];
 }

/**
 * \fn void permutacaoP(int* permutado, int* bloco);
 *
 * \brief Realiza a permutacao com a tabela P
 *
 * \param permutado - ponteiro com informação permutada.
 *        bloco - ponteiro do bloco.
 */
void 
permutacaoP(int* permutado, int* bloco){
	int i;
	int tamanho = (sizeof(p)/sizeof(p[0]));
	for (i = 0; i < tamanho; i++)
		permutado[i] = bloco[p[i] - 1];
}

/**
 * \fn void permutacaoPC1_C(int* permutado, int* bloco);
 *
 * \brief Realiza a permutacao com a tabela pc1_c
 *
 * \param permutado - ponteiro com informação permutada.
 *        bloco - ponteiro do bloco.
 */
 void 
 permutacaoPC1_C(int* permutado, int* bloco){
	int i;
	int tamanho = (sizeof(pc1_c)/sizeof(pc1_c[0]));
	for (i = 0; i < tamanho; i++)
		permutado[i] = bloco[pc1_c[i] - 1];
 }

/**
 * \fn void permutacaoPC1_D(int* permutado, int* bloco);
 *
 * \brief Realiza a permutacao com a tabela pc1_d
 *
 * \param permutado - ponteiro com informação permutada.
 *        bloco - ponteiro do bloco.
 */
 void 
 permutacaoPC1_D(int* permutado, int* bloco){
	int i;
	int tamanho = (sizeof(pc1_d)/sizeof(pc1_d[0]));
	for (i = 0; i < tamanho; i++)
		permutado[i] = bloco[pc1_d[i] - 1];
 }

/**
 * \fn void permutacaoPC2(int* permutado, int* bloco);
 *
 * \brief Realiza a permutacao com a tabela pc2
 *
 * \param permutado - ponteiro com informação permutada.
 *        bloco - ponteiro do bloco.
 */
 void 
 permutacaoPC2(int* permutado, int* bloco){
	int i;
	int tamanho = (sizeof(pc2)/sizeof(pc2[0]));
	for (i = 0; i < tamanho; i++)
		permutado[i] = bloco[pc2[i] - 1];
 }

/**
 * \fn void expande48bits(int* expandido, int* right);
 *
 * \brief Expande os 32 bits em 48 bits.
 *
 * \param expandido - ponteiro com informação expandida.
 *        right - ponteiro com os 32 bits.
 */
 void 
 expande48bits(int* expandido, int* right){
	int tamanho = (sizeof(e)/sizeof(e[0]));
	int i;
	for (i = 0; i < tamanho; i++)
		expandido[i] = right[e[i] - 1];
 }

 /**
  * \fn int getValueS(int linha, int coluna, int tabela);
  *
  * \brief Retorna o valor da tabela na linha e coluna especificada
  *
  * \param linha - linha da tabela.
  *        coluna - coluna da tabela.
  *        tabela - tabela de 1 - 8.
  */
  int 
  getValueS(int linha, int coluna, int tabela){
	switch (tabela){
		case 1:
			return s1[linha][coluna];
			break;
		case 2:
			return s2[linha][coluna];
			break;
		case 3:
			return s3[linha][coluna];
			break;
		case 4:
			return s4[linha][coluna];
			break;
		case 5:
			return s5[linha][coluna];
			break;
		case 6:
			return s6[linha][coluna];
			break;
		case 7:
			return s7[linha][coluna];
			break;
		case 8:
			return s8[linha][coluna];
			break;
	}
  }

 /**
  * \fn int getDesloc(int interacao);
  *
  * \brief Retorna o valor da tabela que é referente ao deslocamento.
  *
  * \param interacao - Identificação do deslocamento.
  */
  int 
  getDesloc(int interacao){
	return desloc[interacao];
  }

