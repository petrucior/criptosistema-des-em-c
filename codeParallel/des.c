/**
 * \file des.cpp
 *
 * \brief Este arquivo contém a implementação dos métodos do criptosistema Data Encryption Standard (DES).
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.2
 * \date Junho 2014
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "des.h"
#include "msg.h"
#include "convertMsg.h"
#include "blocos.h"
#include "permutacao.h"
#include "keys.h"
#include "funcao.h"

/* Checagem de erro */
#ifdef _OPENMP
	#include <omp.h>
#endif

#define TAMANHO 480000 /* Tamanho do texto após o preenchimento com zeros */

int main(int argc, char* argv[]){
	int thread_count = strtol(argv[1], NULL, 10);

	int encriptado[TAMANHO];
	const double tempoInicio = omp_get_wtime();
	des_encrypt(encriptado, thread_count);
	const double tempoFinal = omp_get_wtime();
	char hexadecimal[TAMANHO/4];
	convertBinToHex(hexadecimal, encriptado, TAMANHO);

	/*printf("Encriptado\n");
	int v;
	for (v = 0; v < TAMANHO/4; v++)
		printf("%c", hexadecimal[v]);
	printf("\n");*/

	int decriptado[TAMANHO];
	des_decrypt(decriptado, encriptado, TAMANHO);
	convertBinToHex(hexadecimal, decriptado, TAMANHO);

	/*printf("Decriptado\n");
	for (v = 0; v < TAMANHO/4; v++)
		printf("%c", hexadecimal[v]);
	printf("\n");*/
	
	printf("tempo percorrido: %f\n", tempoFinal - tempoInicio);

	return 0;
}

/**
 * \fn void des_encrypt(int* encriptado, int thread_count);
 *
 * \brief Método de encriptação.
 *
 * \param encriptado - ponteiro com informação criptografada.
 *        thread_count - quantidade de threads.
 */
 void 
 des_encrypt(int* encriptado, int thread_count){
	/* Definição dos lock's*/
	omp_lock_t l0;
	omp_lock_t l1;
	omp_lock_t l2;
	omp_lock_t l3;
	omp_lock_t l4;
	omp_lock_t l5;
	omp_lock_t l6;
	omp_lock_t l7;
	omp_lock_t l8;
	omp_lock_t l9;
	omp_lock_t l10;
	omp_lock_t l11;
	omp_lock_t l12;
	omp_lock_t l13;
	omp_lock_t l14;
	omp_lock_t l15;

	/* Iniciando os lock's */
	omp_init_lock(&l0);
	omp_init_lock(&l1);
	omp_init_lock(&l2);
	omp_init_lock(&l3);
	omp_init_lock(&l4);
	omp_init_lock(&l5);
	omp_init_lock(&l6);
	omp_init_lock(&l7);
	omp_init_lock(&l8);
	omp_init_lock(&l9);
	omp_init_lock(&l10);
	omp_init_lock(&l11);
	omp_init_lock(&l12);
	omp_init_lock(&l13);
	omp_init_lock(&l14);
	omp_init_lock(&l15);
	
	int i;

	/* Convertendo o texto para binário */
	int tam = (int) strlen(mensagem)*8;
	int binario[tam];
	convertTextToBin(binario, mensagem, tam/8);
	
	/* Quebrando a mensagem binária em blocos */
	int tamanho = 0;
	if (tam % 64 != 0) tamanho = (int) tam + (64 - (tam % 64));
	else tamanho = tam;
	int bls[tamanho];
	quebraBin2Bls(bls, binario, tam);

	/* Construção de subchaves */
	setSubKeys();	

	/* Para cada bloco é realizada a encriptação */
#	pragma omp parallel for num_threads(thread_count)
	for (i = 0; i < tamanho/64; i++){
		/* Preenchimento de cada bloco */
		int bloco[64];
		extrairBls(bloco, bls, i);
		
		/* Permutação inicial */
		int perm[64];
		permutacaoInicial(perm, bloco);

		/* Quebra o resultado permutado em duas partes: left e right */
		int left[32], right[32];
		quebra32bits(left, right, perm, 0);

		/* Bloco pipeline */
		omp_set_lock(&l0);
			pipelineEncrypt(left, right, 0);
		omp_unset_lock(&l0);
		omp_set_lock(&l1);
			pipelineEncrypt(left, right, 1);
		omp_unset_lock(&l1);
		omp_set_lock(&l2);
			pipelineEncrypt(left, right, 2);
		omp_unset_lock(&l2);
		omp_set_lock(&l3);
			pipelineEncrypt(left, right, 3);
		omp_unset_lock(&l3);
		omp_set_lock(&l4);
			pipelineEncrypt(left, right, 4);
		omp_unset_lock(&l4);
		omp_set_lock(&l5);
			pipelineEncrypt(left, right, 5);
		omp_unset_lock(&l5);
		omp_set_lock(&l6);
			pipelineEncrypt(left, right, 6);
		omp_unset_lock(&l6);
		omp_set_lock(&l7);
			pipelineEncrypt(left, right, 7);
		omp_unset_lock(&l7);
		omp_set_lock(&l8);
			pipelineEncrypt(left, right, 8);
		omp_unset_lock(&l8);
		omp_set_lock(&l9);
			pipelineEncrypt(left, right, 9);
		omp_unset_lock(&l9);
		omp_set_lock(&l10);
			pipelineEncrypt(left, right, 10);
		omp_unset_lock(&l10);
		omp_set_lock(&l11);
			pipelineEncrypt(left, right, 11);
		omp_unset_lock(&l11);
		omp_set_lock(&l12);
			pipelineEncrypt(left, right, 12);
		omp_unset_lock(&l12);
		omp_set_lock(&l13);
			pipelineEncrypt(left, right, 13);
		omp_unset_lock(&l13);
		omp_set_lock(&l14);
			pipelineEncrypt(left, right, 14);
		omp_unset_lock(&l14);
		omp_set_lock(&l15);
			pipelineEncrypt(left, right, 15);
		omp_unset_lock(&l15);
		
		/* Concatenacao com swap de right com left (preoutput) */
		int concatenado[64];
		concatenacao(concatenado, right, left, 32, 32);

		/* Permutação final */
		int permutF[64];
		permutacaoFinal(permutF, concatenado);

		/* Se a quantidade de threads é superior que a quantidade de trabalho */
		/*if (thread_count >= tamanho/64 - 1){*/
			/* Concatenando com o ponteiro de saída encriptada */
		/*#	pragma omp critical
			concatenacao(encriptado, encriptado, permutF, 64*omp_get_thread_num(), 64);*/ /* contador modificado por myrank*/
		/*}
		else {
		#	pragma omp critical*/
			concatenacao(encriptado, encriptado, permutF, 64*i, 64);
		/*}*/
	} /* Termino do laço for */

	/* Destruição dos lock's */
	omp_destroy_lock(&l0);
	omp_destroy_lock(&l1);
	omp_destroy_lock(&l2);
	omp_destroy_lock(&l3);
	omp_destroy_lock(&l4);
	omp_destroy_lock(&l5);
	omp_destroy_lock(&l6);
	omp_destroy_lock(&l7);
	omp_destroy_lock(&l8);
	omp_destroy_lock(&l9);
	omp_destroy_lock(&l10);
	omp_destroy_lock(&l11);
	omp_destroy_lock(&l12);
	omp_destroy_lock(&l13);
	omp_destroy_lock(&l14);
	omp_destroy_lock(&l15);
 }

/**
 * \fn void des_decrypt(int* decriptado, int* encriptado, int tamanho);
 *
 * \brief Método de encriptação.
 *
 * \param decriptado - ponteiro com a mensagem decriptada.
 *         encriptado - ponteiro com informação criptografada.
 *         tamanho - Tamanho da mensagem encriptada.
 */
 void 
 des_decrypt(int* decriptado, int* encriptado, int tamanho){
	/* Quebrando a mensagem binária em blocos */
	int bls[tamanho];
	quebraBin2Bls(bls, encriptado, tamanho);

	/* Construção de subchaves */
	setSubKeys();

	int bloco[64];
	int contador = 0;
	int i, j;
	/* Para cada bloco é realizada a decriptação */
	for (i = 0; i < tamanho/64; i++){
		/* Preenchimento de cada bloco */
		extrairBls(bloco, bls, i);

		/* Permutação inicial */
		int perm[64];
		permutacaoInicial(perm, bloco);

		/* Quebra o resultado permutado em duas partes: right e left */
		int left[32], right[32];
		quebra32bits(right, left, perm, 0);

		for (j = 0; j < 16; j++){
			/* Pegando a subChave */
			int subChave[48];
			getSubKey(subChave, 16 - j - 1);

			/* Função de feistel */
			int saidaFeistel[32];
			f(saidaFeistel, left, subChave);

			/* Função xor */
			int aux[32];
			copiar(aux, left, 32);
			operacaoXor(left, right, saidaFeistel, 32);
			copiar(right, aux, 32);
		}
		
		/* Concatenacao com swap de left com right (preoutput) */
		int concatenado[64];
		concatenacao(concatenado, left, right, 32, 32);

		/* Permutação final */
		int permutF[64];
		permutacaoFinal(permutF, concatenado);

		/* Concatenando com o ponteiro de saída encriptada */
		concatenacao(decriptado, decriptado, permutF, 64*contador, 64);
		
		contador++;
	} /* Termino do laço for */
 }

 /**
  * \fn void pipelineEncrypt(int* left, int* right, int posicao);
  *
  * \brief Processo de paralelização com o método pipeline.
  *
  * \param left - ponteiro com 32 bits mais à esquerda.
  *        right - ponteiro com 32 bits mais à direita.
  *        posicao - valor de onde terei que pegar a chave.
  */
  void 
  pipelineEncrypt(int* left, int* right, int posicao){
	/* Pegando a subChave */
	int subChave[48];
	getSubKey(subChave, posicao);

	/* Função de feistel */
	int saidaFeistel[32];
	f(saidaFeistel, right, subChave);

	/* Função xor */
	int aux[32];
	copiar(aux, right, 32);
	operacaoXor(right, left, saidaFeistel, 32);
	copiar(left, aux, 32);
  }
