/**
 * \file convertMsg.h
 *
 * \brief Este arquivo é responsável por realizar conversões das mensagens.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Maio 2014
 */

#ifndef _CONVERTMSG_H_
#define _CONVERTMSG_H_

#include <stdlib.h>
#include <stdio.h>

 /**
  * \fn void convertMsgToBin(int* binario, int valor, int tamanho);
  *
  * \brief Função que converte um inteiro em binário.
  *
  * \param binario - ponteiro de um vetor de inteiros onde armazenará o valor binário.
  *        valor - contém o valor que desejamos converter para binário.
  *        tamanho - quantidade de bits a ser colocado no vetor binário.
  *
  * \return O endereço de vetor correspondente a representação em binário do valor inteiro.
  */
  void convertMsgToBin(int* binario, int valor, int tamanho);

 /**
  * \fn void convertTextToBin(int* textoBinario, char* texto, int tamanhoTexto);
  *
  * \brief Função que converte um texto em binário.
  *
  * \param textoBinario - ponteiro com saída do texto binário.
  *        texto - contém a informação que desejamos converter para binário.
  *        tamanho - a quantidade de caracteres do texto.
  *
  * \return O endereço de vetor correspondente a representação em binário do texto.
  */
  void convertTextToBin(int* textoBinario, char* texto, int tamanhoTexto);

 /**
  * \fn int convertBinToNum(char* binario, int tamanho);
  *
  * \brief Função que converte de binário para inteiro.
  *
  * \param binario - contém a informação que desejamos converter para binário.
  *        tamanho - a quantidade de bits contida no binário.
  *
  * \return O valor convertido em inteiro.
  */
  int convertBinToNum(int* binario, int tamanho);

 /**
  * \fn void extraiInfo(int* vetor, int* binario, int posicao);
  *
  * \brief Pegar informação de um vetor binário.
  *
  * \param vetor - ponteiro onde colocaremos a informação extraida.
  *        binario - contém a informação que desejamos pegar.
  *        posicao - lugar de onde vamos começar a pegar a informação.
  */
  void extraiInfo(int* vetor, int* binario, int posicao);

 /**
  * \fn void convertBinToHex(char* hexadecimal, char* binario, int tamanho);
  *
  * \brief Função que converte de binário para hexadecimal.
  *
  * \param hexadecimal - ponteiro onde colocaremos a informação convertida.
  *        binario - contém a informação que desejamos converter para binário.
  *        tamanho - a quantidade de bits contida no binário.
  */
  void convertBinToHex(char* hexadecimal, int* binario, int tamanho);

#endif

