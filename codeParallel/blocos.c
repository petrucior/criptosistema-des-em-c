/**
 * \file blocos.c
 *
 * \brief Este arquivo contém os blocos da mensagem.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.2
 * \date Maio 2014
 */

#include "blocos.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/**
 * \fn void quebraBin2Bls(int* blocos, int* binario, int tamanho);
 *
 * \brief Função que quebra a informação binária em blocos.
 *
 * \param blocos - ponteiro onde vamos adicionar os vetores binários.
 *        binario - ponteiro com informação binária.
 *
 * \return Quebra a informação binária em blocos de 64 bits para ser
 * utilizado no DES.
 */
 void 
 quebraBin2Bls(int* blocos, int* binario, int tamanho){
	int contador = 0;
	if (tamanho % 64 == 0){
		/*printf("\nmultiplo de 64\n");*/
		while (contador < tamanho){
			blocos[contador] = binario[contador];
			/*if (contador % 64 == 0) printf("\n");
			printf("%d", blocos[contador]);*/
			contador++;
		}
	}
	else{
		/*printf("\nnao multiplo de 64\n");*/
		/*Preenchendo os blocos com 1's*/
		int tamBlocos = (int) tamanho + (64 - (tamanho % 64));
		while (contador < tamBlocos){
			if (contador < tamanho * 8 ){
				blocos[contador] = binario[contador];
			}
			else{
				blocos[contador] = 1;
			}
			contador++;
		}
	}
	/*printf("\n");*/
 }


/**
 * \fn void quebra32bits(int* left, int* right, int* blocos, interacao);
 *
 * \brief Função que quebra o bloco em dois subblocos de 32 bits.
 *
 * \param left - ponteiro para armazenar os 32 bits mais a esquerda.
 *        right - ponteiro para armazenar os 32 bits mais a direita.
 *        blocos - ponteiro onde vamos adicionar os vetores binários.
 *        interacao - identificador de qual bloco será quebrado.
 *
 * \return Quebra a informação binária em dois blocos de 32 bits para ser utilizado no DES.
 */
 void 
 quebra32bits(int* left, int* right, int* blocos, int interacao){
	int i;
	for (i = 0; i < 64; i++){
		if (i < 32) left[i] = blocos[i + 64*interacao];
		else right[i - 32] = blocos[i + 64*interacao];
	}
 }

/**
 * \fn void quebraBin2Bls(int* bloco, int* blocos, int posicao);
 *
 * \brief Extrai o bloco especificado por posicao.
 *
 * \param bloco - ponteiro onde vamos adicionar os vetores binários.
 *        blocos - ponteiro com informação binária.
 *        posicao - lugar de onde vamos começar a pegar a informação.
 */
 void 
 extrairBls(int* bloco, int* blocos, int posicao){
	int i;
	for (i = 0; i < 64; i++)
		bloco[i] = blocos[i + 64*posicao];
 }

