/**
 * \file des.h
 *
 * \brief Este arquivo contém as assinaturas dos métodos do criptosistema Data Encryption Standard (DES).
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Junho 2014
 */

#ifndef _DES_H_
#define _DES_H_

#include <stdlib.h>
#include <stdio.h>

 /**
  * \fn void des_encrypt(int* encriptado, int thread_count);
  *
  * \brief Método de encriptação.
  *
  * \param encriptado - ponteiro com informação criptografada.
  *        thread_count - quantidade de threads.
  */
  void des_encrypt(int* encriptado, int thread_count);

 /**
  * \fn void des_encrypt(int* decriptado, int* encriptado, int tamanho);
  *
  * \brief Método de encriptação.
  *
  * \param decriptado - ponteiro com a mensagem decriptada.
  *         encriptado - ponteiro com informação criptografada.
  *         tamanho - Tamanho da mensagem encriptada.
  */
  void des_decrypt(int* decriptado, int* encriptado, int tamanho);

 /**
  * \fn void pipelineEncrypt(int* left, int* right, int posicao);
  *
  * \brief Processo de paralelização com o método pipeline.
  *
  * \param left - ponteiro com 32 bits mais à esquerda.
  *        right - ponteiro com 32 bits mais à direita.
  */
  void pipelineEncrypt(int* left, int* right, int posicao);

#endif
