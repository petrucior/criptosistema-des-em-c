/**
 * \file permutacao.h
 *
 * \brief Este arquivo contém as funções de permutação.
 *
 * \author 
 * Petrucio Ricardo Tavares de Medeiros \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * petrucior at gmail (dot) com
 *
 * \version 0.1
 * \date Junho 2014
 */

#ifndef _PERMUTACAO_H_
#define _PERMUTACAO_H_


#include <stdlib.h>
#include <stdio.h>

 /**
  * \fn void permutacaoInicial(int* permutado, int* bloco);
  *
  * \brief Realiza a permutacao inicial
  *
  * \param permutado - ponteiro com informação permutada.
  *        bloco - ponteiro do bloco.
  */
  void permutacaoInicial(int* permutado, int* bloco);

 /**
  * \fn void permutacaoFinal(int* permutado, int* bloco);
  *
  * \brief Realiza a permutacao final
  *
  * \param permutado - ponteiro com informação permutada.
  *        bloco - ponteiro do bloco.
  */
  void permutacaoFinal(int* permutado, int* bloco);

 /**
  * \fn void permutacaoP(int* permutado, int* bloco);
  *
  * \brief Realiza a permutacao com a tabela P
  *
  * \param permutado - ponteiro com informação permutada.
  *        bloco - ponteiro do bloco.
  */
  void permutacaoP(int* permutado, int* bloco);

 /**
  * \fn void permutacaoPC1_C(int* permutado, int* bloco);
  *
  * \brief Realiza a permutacao com a tabela PC1_C
  *
  * \param permutado - ponteiro com informação permutada.
  *        bloco - ponteiro do bloco.
  */
  void permutacaoPC1_C(int* permutado, int* bloco);

 /**
  * \fn void permutacaoPC1_D(int* permutado, int* bloco);
  *
  * \brief Realiza a permutacao com a tabela PC1_D
  *
  * \param permutado - ponteiro com informação permutada.
  *        bloco - ponteiro do bloco.
  */
  void permutacaoPC1_D(int* permutado, int* bloco);

 /**
  * \fn void permutacaoPC2(int* permutado, int* bloco);
  *
  * \brief Realiza a permutacao com a tabela PC2
  *
  * \param permutado - ponteiro com informação permutada.
  *        bloco - ponteiro do bloco.
  */
  void permutacaoPC2(int* permutado, int* bloco);

 /**
  * \fn void expande48bits(int* expandido, int* right);
  *
  * \brief Expande os 32 bits em 48 bits.
  *
  * \param expandido - ponteiro com informação expandida.
  *        right - ponteiro com os 32 bits.
  */
  void expande48bits(int* expandido, int* right);

 /**
  * \fn int getValueS(int linha, int coluna, int tabela);
  *
  * \brief Retorna o valor da tabela na linha e coluna especificada
  *
  * \param linha - linha da tabela.
  *        coluna - coluna da tabela.
  *        tabela - tabela de 1 - 8.
  */
  int getValueS(int linha, int coluna, int tabela);

 /**
  * \fn int getDesloc(int interacao);
  *
  * \brief Retorna o valor da tabela que é referente ao deslocamento.
  *
  * \param interacao - Identificação do deslocamento.
  */
  int getDesloc(int interacao);

#endif
